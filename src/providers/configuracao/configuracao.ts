import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
/*
  Generated class for the ConfiguracaoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConfiguracaoProvider {

  public serverHttp: string = "http://127.0.0.1:8000/api/";
  // public serverHttp: string = "https://pdall.fuston.com.br/api/";

  constructor(public http: Http) {
    console.log('Hello ConfiguracaoProvider Provider');
  }

  public handleError(error) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('Um erro ocorreu:', error.error.message);
      return new ErrorObservable({error: true, description: error.error.message});
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong
      console.error('ErrorConf', error);
      if(error.status == 404 || error.status == 500) {
        return new ErrorObservable({error: true, description: 'Erro no servidor, por favor entre em contato com o suporte.'});
      } else if(error.status == 418) {
        return new ErrorObservable(JSON.parse(error._body));
      }
    }
  };
  
}
