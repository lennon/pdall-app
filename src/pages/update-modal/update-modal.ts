import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { ConfiguracaoProvider } from '../../providers/configuracao/configuracao';

/**
 * Generated class for the UpdateModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-update-modal',
  templateUrl: 'update-modal.html',
})
export class UpdateModalPage {

  options : InAppBrowserOptions = {
    location : 'no',//Or 'no' 
    hidden : 'no', //Or  'yes'
    clearcache : 'yes',
    clearsessioncache : 'yes',
    zoom : 'yes',//Android only, shows browser zoom controls 
    hardwareback : 'no',
    mediaPlaybackRequiresUserAction : 'no',
    shouldPauseOnSuspend : 'no', //Android only 
    closebuttoncaption : 'X', //iOS only
    disallowoverscroll : 'yes', //iOS only
    footer: 'no', 
    toolbar : 'no', //iOS only 
    enableViewportScale : 'no', //iOS only 
    allowInlineMediaPlayback : 'no',//iOS only 
  };

  public linkAndroid = 'https://play.google.com/store/apps/details?id=br.com.quemmequer';
  public linkIos = 'https://play.google.com/store/apps/details?id=br.com.quemmequer';
  public linkPdall = 'https://pdall.fuston.com.br';

  public os: string;

  constructor(public conf: ConfiguracaoProvider, private theInAppBrowser: InAppBrowser, public statusBar: StatusBar, public navCtrl: NavController, public navParams: NavParams) {
    this.statusBar.styleLightContent();
  }

  public openWithInAppBrowser(){
    this.theInAppBrowser.create(this.linkPdall, "_self", this.options);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UpdateModalPage');
  }

}
