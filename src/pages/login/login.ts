import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { ConfiguracaoProvider } from '../../providers/configuracao/configuracao';
import { catchError, retry } from 'rxjs/operators';
import { UpdateModalPage } from '../update-modal/update-modal';
import 'rxjs/add/operator/map';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public objLogin: any = { username: '', password: '' }
  public checkUpdateInterval: any;

  constructor(public modalCtrl: ModalController, public conf: ConfiguracaoProvider, public loading: LoadingController, public alert: AlertController, public navCtrl: NavController, public navParams: NavParams) {}

  ionViewCanEnter() {
    console.log('ionViewCanEnter LoginPage');
    if(this.checkUpdateInterval) { clearInterval(this.checkUpdateInterval); }
    this.checkUpdateInterval = setInterval(() => {console.log('checkUpdate'); this.checkUpdate();}, 10000);
    this.checkUpdate();
  }

  checkUpdate() {
    this.conf.http.post(this.conf.serverHttp+'v1/check/version/', JSON.stringify(this.objLogin))
    .map((res) => res.json())
    .pipe(
      retry(2),
      catchError(this.conf.handleError)
    )
    .subscribe((success) => {
      console.log(success);
    }, (error) => {
      console.error('Error', error);
      this.conf.http.post(this.conf.serverHttp+'v1/check/version/new/', JSON.stringify(this.objLogin))
      .map((res) => res.json())
      .pipe(
        retry(2),
        catchError(this.conf.handleError)
      )
      .subscribe((success) => {
        console.log(success);
      }, (error) => {
        console.error('Error', error);
        if(this.checkUpdateInterval) { clearInterval(this.checkUpdateInterval); }
        // AQUI APARECER O MODAL
        let modal = this.modalCtrl.create(UpdateModalPage, {os: 'ios'});
        modal.present();
      });
    });
  }

  recuperarSenha() {
    this.alert.create({
      title: 'Recuperar Senha',
      subTitle: 'Insira seu username',
      inputs: [
        {
          name: 'username',
          placeholder: 'Username'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Recuperar',
          handler: data => {
            if(data.username) {
              let loading = this.loading.create({
                duration: 2000
              });
              loading.onDidDismiss(() => {
                this.alert.create({
                  title: 'Recuperar Senha',
                  subTitle: 'Foi enviado um e-mail para você com o próximo passo para recuperar sua senha',
                  buttons: ["OK"]
                }).present();
              });
              loading.present();
            } else {
              this.alert.create({
                title: 'Recuperar Senha',
                subTitle: 'É necessário informar seu username',
                buttons: ["OK"]
              }).present();
            }
          }
        }
      ]
    }).present();
  }

  entrar() {
    if(!this.objLogin.username) {
      this.alert.create({
        title: 'Aviso',
        subTitle: 'Username não pode ser vazio',
        buttons: ["OK"]
      }).present();
      return false;
    }
    if(!this.objLogin.password) {
      this.alert.create({
        title: 'Aviso',
        subTitle: 'Password não pode ser vazio',
        buttons: ["OK"]
      }).present();
      return false;
    }

    let loading = this.loading.create({dismissOnPageChange: true});
    loading.present();
    this.conf.http.post(this.conf.serverHttp+'v1/login/', JSON.stringify(this.objLogin))
    .map((res) => res.json())
    .pipe(
      retry(2),
      catchError(this.conf.handleError)
    )
    .subscribe((success) => {
      console.log(success);
      loading.dismiss();
      this.navCtrl.setRoot('InicioPage');
    }, (error) => {
      loading.dismiss();
      console.error('Error', error);
      if(error.error) {
        this.alert.create({
          title: 'Aviso',
          subTitle: error.description,
          buttons: ['OK']
        }).present();
      }
    });
    
  }

}
